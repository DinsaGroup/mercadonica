// Main javascript project for DINAMICDIN
$(document).ready(function () {
	//Validate input number quantity
	$('.only_numbers').keydown(function(e){

		// Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [ 8, 46, 9, 27, 13,188,110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
		// Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

	});

    //Validate only integer input
    $('.only_integer').keydown(function(e){

        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [ 8, 46, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });

    // Preformar los valores con dos decimales
    $('.only_numbers').focusout(function(){
        if($(this).val() != ""){
            $(this).val(parseFloat($(this).val()).toFixed(2));
        }
    });

    // Calcular utilidad
    $('.percent').focusout(function(){
        if( ($(this).val() != "") && ($('.cost').val() != "") ){
            var cost = parseFloat($('.cost').val());
            var percent = parseFloat($(this).val()/100);
            var total = parseFloat(cost * percent + cost).toFixed(2);
            $('.price').val(total);
        } 
    });

    $('.percent').keypress(function(event){
          if ( event.which == 13 ) {
                event.preventDefault();
                if( ($(this).val() != "") && ($('.cost').val() != "") ){
                    var cost = parseFloat($('.cost').val());
                    var percent = parseFloat($(this).val()/100);
                    var total = parseFloat(cost * percent + cost).toFixed(2);
                    $('.price').val(total);
                }
          }
             
    });


    $('.price').focusout(function(){
        if( $('.cost').val() != "" ){
            var cost = parseFloat($('.cost').val());
            var price = parseFloat($(this).val() * 100);
            var total = parseFloat(price/cost - 100).toFixed(2);
            $('.percent').val(total);
        } 
    });

    $('.price').keypress(function(event){
          if ( event.which == 13 ) {
                event.preventDefault();
                if( $('.cost').val() != "" ){
                    var cost = parseFloat($('.cost').val());
                    var price = parseFloat($(this).val() * 100);
                    var total = parseFloat(price/cost - 100).toFixed(2);
                    $('.percent').val(total);
                } 
          }
             
    });

    // Ocultar mensajes de alerta
    $('.alert').delay(3000).fadeOut('slow');


})