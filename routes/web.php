<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/', function () {
    return view('welcome');
});

// Set up Administrator Access
Route::group(['prefix' => 'ControlPanel/Admin', 'middleware' => ['auth', 'role:superadmin|manager|support'], 'namespace' => 'backend\controlpanel'], function () {

	// Dashboard
	Route::resource('/', 'DashboardController');
	Route::resource('status', 'StatusController');
	Route::resource('currencies', 'CurrenciesController');
	Route::resource('job-titles', 'JobTitlesController');
	Route::resource('taxes', 'TaxesController');
	Route::resource('bill-types', 'BillTypesController');

});

// Set up Clients Portal access
Route::group(['prefix' => 'Portal/Admin', 'middleware' => ['auth', 'role:superadmin|manager|support|client'], 'namespace' => 'backend\portalpanel'], function () {
	// Dashboard
	Route::resource('/', 'DashboardController');
});
