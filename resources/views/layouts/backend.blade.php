 <!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<title>@yield('title') | MercadoNica  </title>

    	<!-- Bootstrap -->
    	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.5/flatly/bootstrap.min.css" rel="stylesheet">

    	<!-- Icons -->
    	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel='stylesheet' type='text/css'>

    	<!-- Estilos del sitio -->
    	<!-- Morris -->
        <link href="{{{ asset('css/backend/morris-0.4.3.min.css') }}}" rel="stylesheet">

        <!-- Gritter -->
        <link href="{{{ asset('css/backend/jquery.gritter.css') }}}" rel="stylesheet">

        <!-- Data Tables -->
        <link href="{{{ asset('css/backend/dataTables.bootstrap.css') }}}" rel="stylesheet">
        <link href="{{{ asset('css/backend/dataTables.responsive.css') }}}" rel="stylesheet">
        <link href="{{{ asset('css/backend/dataTables.tableTools.min.css') }}}" rel="stylesheet">

        <link href="{{{ asset('css/backend/animate.css') }}}" rel="stylesheet">
        <link href="{{{ asset('css/backend/style.css') }}}" rel="stylesheet">

        @yield('css')

    </head>
    <body class="mini-navbar">
        @if(Session::has('success_message'))
            <div class="alert alert-success">
                <i class="fa fa-check"></i>
                {{ Session::get('success_message') }}
            </div>
        @endif

        @if(Session::has('error_message'))
            <div class="alert alert-danger">
                <i class="fa fa-belt"></i>
                {{ Session::get('error_message') }}
            </div>
        @endif

    	<div id="wrapper">

    		<nav class="navbar-default navbar-static-side" role="navigation">

    			<div class="sidebar-collapse">
                        <ul class="nav metismenu" id="side-menu">
                            
                            <!-- Perfil de usuario -->
                            <li class="nav-header">
                                <div class="dropdown profile-element">
                                	<img src="{{{asset('images/patterns/header-profile.png') }}}" class="top-admin-image" />
                                	
                                </div>

                                <div class="logo-element">
                                    <img src="{{{asset('../images/patterns/mini-icon.png') }}}" width="60">
                                </div>
                            </li> <!-- /.nav-header -->
                            
                            @include('backend.controlpanel.partials.leftmenu')

                        </ul>
                </div> <!-- /.sidebar collapse -->

    		</nav>

    		<div id="page-wrapper" class="basecolor">

    			@include('backend.controlpanel.partials.topmenu')

    			@yield('content')
    			
    			@include('backend.controlpanel.partials.footer')
    		
            </div>

    	</div>

    	<!-- Scripts Originales -->
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

    	<!-- Mainly scripts -->
        <script src="{{{asset('js/backend/inspinia.js') }}}"></script>
        <script src="{{{asset('js/main.js') }}}"></script>

        <script src="{{{asset('js/backend/metisMenu/jquery.metisMenu.js') }}}"></script>
        <script src="{{{asset('js/backend/slimscroll/jquery.slimscroll.min.js') }}}"></script> 

        <!-- jQuery UI -->
        <script src="{{{asset('js/backend/jquery-ui.min.js') }}}"></script>

        <!-- Imput Mask -->
        <script type="text/javascript" src="{{{ asset('js/inputmask.js') }}}"></script>

        @yield('javascript')
       
    </body>
</html>
