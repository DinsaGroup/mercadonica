@extends('layouts.backend')

@section('title') 
	{{ $title }}
@endsection 

@section('content')
<!-- Header page / Titulo de la seccion -->
<div class="row wrapper page-heading"> 
    <div class="col-xs-12 col-lg-8">
	    <h1>{{ $title }}</h1>
	    <small>Detalle del comportamiento de la empresa, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
    </div>
    <div class="col-xs-12 col-lg-4 text-right">
    	<h3 style="margin: 35px 0 0 0">DinamicDIN v 1.0</h3>
    	<small>Control de Sistema Maestro</small>
    </div>

</div>

<div class="wrapper wrapper-content">
	{{-- Comportamiento ventas --}}
   	<div class="row">
        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 386.30 K</h2>
                    <div class="stat-percent font-bold text-warning">12% <i class="fa fa-bolt"></i></div>
                    <small>Total ventas hoy</small>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 1.58 M</h2>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>Total ventas Semana</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 35.12 M</h2>
                    <div class="stat-percent font-bold text-success">8% <i class="fa fa-bolt"></i></div>
                    <small>Total ventas mes</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 80,80 M</h2>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>Total venta año</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 415.80 K</h2>
                    <div class="stat-percent font-bold text-info">2% <i class="fa fa-level-up"></i></div>
                    <small>Promedio venta diaria</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 4.5 M</h2>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-down"></i></div>
                    <small>Cartera crédito</small>
                </div>
            </div>
        </div>
    </div>

    {{-- Grafos --}}
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div>
                        <span class="pull-right text-right">
                        <small>Promedio de ventas diarias: <strong>Nicaragua</strong></small>
                            <br/>
                            Ventas por días: U$ 415.80 K
                        </span>
                        <h3 class="font-bold no-margins">
                            Comparación de ventas por año
                        </h3>
            			<small>Datos expresados en miles.</small>
                    </div>

                    <div class="m-t-sm">

                        <div class="row">
                            <div class="col-md-8">
                                <div>
                                    <canvas id="lineChart" height="170"></canvas>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <ul class="stat-list m-t-lg">
                                    <li>
                                        <h2 class="no-margins">U$ 80.80 M</h2>
                                        <small>Total ventas 2016</small>
                                        <div class="progress progress-mini">
                                            <div class="progress-bar" style="width: 48%;"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <h2 class="no-margins ">U$ 96.57 M</h2>
                                        <small>Total de ventas 2015</small>
                                        <div class="progress progress-mini">
                                            <div class="progress-bar" style="width: 60%;"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="m-t-md">
                        <small class="pull-right">
                            <i class="fa fa-clock-o"> </i>
                            Update on 16.07.2015
                        </small>
                        <small>
                            <strong>Analysis of sales:</strong> The value has been changed over time, and last month reached a level over $50,000.
                        </small>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="ibox float-e-margins">

            	<div class="ibox-content">
                    <div class="row">
                    	<div class="col-xs-12">
                    	 	<h3>Tienda 1</h3>
                    	</div>
                        <div class="col-xs-4">
                            <small class="stats-label">Factura dia</small>
                            <h4>U$ 125.51 K</h4>
                        </div>

                        <div class="col-xs-4">
                            <small class="stats-label">Factura Mes</small>
                            <h4>U$ 954.51 K</h4>
                        </div>
                        <div class="col-xs-4">
                            <small class="stats-label">Margen ganancia</small>
                            <h4>U$ 100.51 K </h4>
                        </div>
                    </div>
                </div>

            	<div class="ibox-content">
                    <div class="row">
                    	<div class="col-xs-12">
                    	 	<h3>Tienda 2</h3>
                    	</div>
                        <div class="col-xs-4">
                            <small class="stats-label">Factura dia</small>
                            <h4>U$ 251.51 K</h4>
                        </div>

                        <div class="col-xs-4">
                            <small class="stats-label">Factura Mes</small>
                            <h4>U$ 1.45 M</h4>
                        </div>
                        <div class="col-xs-4">
                            <small class="stats-label">Margen ganancia</small>
                            <h4>U$ 250.51 K </h4>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="row">
                    	<div class="col-xs-12">
                    	 	<h3>Tienda 3</h3>
                    	</div>
                        <div class="col-xs-4">
                            <small class="stats-label">Factura dia</small>
                            <h4>U$ 100.51 K</h4>
                        </div>

                        <div class="col-xs-4">
                            <small class="stats-label">Factura Mes</small>
                            <h4>U$ 750.51 K</h4>
                        </div>
                        <div class="col-xs-4">
                            <small class="stats-label">Margen ganancia</small>
                            <h4>U$ 60.51 K </h4>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>

    {{-- Comportamiento productos --}}
    <div class="row">
        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins"> 386.30 K</h2>
                    <div class="stat-percent font-bold text-warning">12% <i class="fa fa-level-down"></i></div>
                    <small>Marca 1</small>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 1.58 M</h2>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>Marca 2</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 35.12 M</h2>
                    <div class="stat-percent font-bold text-success">8% <i class="fa fa-bolt"></i></div>
                    <small>Marca 3</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 80,80 M</h2>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>Marca 4</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 415.80 K</h2>
                    <div class="stat-percent font-bold text-info">2% <i class="fa fa-level-up"></i></div>
                    <small>Marca 5</small>
                </div>
            </div>
        </div>

        <div class="col-md-2">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2 class="no-margins">U$ 4.5 M</h2>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-down"></i></div>
                    <small>Marca 6</small>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    	{{-- Pedidos en linea --}}
    	<div class="col-xs-12 col-md-4">
    		<div class="ibox">

    			<div class="ibox-title">
		            <h5>Pedidos en línea</h5>
		            <div class="ibox-tools">
		                <a class="collapse-link">
		                    <i class="fa fa-chevron-up"></i>
		                </a>
		                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		                    <i class="fa fa-wrench"></i>
		                </a>
		                <ul class="dropdown-menu dropdown-user">
		                    <li><a href="#">Config option 1</a>
		                    </li>
		                    <li><a href="#">Config option 2</a>
		                    </li>
		                </ul>
		                <a class="close-link">
		                    <i class="fa fa-times"></i>
		                </a>
		            </div>
		        </div>


		        <div class="ibox-content">
		            <div class="row">
		                
		                <div class="col-xs-12">
		                    <div class="input-group">
		                    	<input type="text" placeholder="Search" class="input-sm form-control"> 
		                    	<span class="input-group-btn">
		                        	<button type="button" class="btn btn-sm btn-primary"> Go!</button> 
		                        </span>
		                    </div>
		                </div>
		            </div>

		            <hr>

		            <div class="table-responsive">
		                <table class="table table-striped">
		                    <thead>
		                    <tr>

		                        <th>#</th>
		                        <th>Project </th>
		                        <th>Name </th>
		                        <th>Phone </th>
		                        <th>Company </th>
		                        <th>Completed </th>
		                        <th>Task</th>
		                        <th>Date</th>
		                        <th>Action</th>
		                    </tr>
		                    </thead>
		                    <tbody>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>3</td>
		                        <td>Betha project</td>
		                        <td>John Smith</td>
		                        <td>0800 1111</td>
		                        <td>Erat Volutpat</td>
		                        <td><span class="pie">3,1</span></td>
		                        <td>75%</td>
		                        <td>Jul 18, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>3</td>
		                        <td>Betha project</td>
		                        <td>John Smith</td>
		                        <td>0800 1111</td>
		                        <td>Erat Volutpat</td>
		                        <td><span class="pie">3,1</span></td>
		                        <td>75%</td>
		                        <td>Jul 18, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    </tbody>
		                </table>
		            </div>

		        </div>
    		
    		</div>
    	</div>

    	{{-- Pedidos en linea --}}
    	<div class="col-xs-12 col-md-4">
    		<div class="ibox">

    			<div class="ibox-title">
		            <h5>Últimas facturas</h5>
		            <div class="ibox-tools">
		                <a class="collapse-link">
		                    <i class="fa fa-chevron-up"></i>
		                </a>
		                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		                    <i class="fa fa-wrench"></i>
		                </a>
		                <ul class="dropdown-menu dropdown-user">
		                    <li><a href="#">Config option 1</a>
		                    </li>
		                    <li><a href="#">Config option 2</a>
		                    </li>
		                </ul>
		                <a class="close-link">
		                    <i class="fa fa-times"></i>
		                </a>
		            </div>
		        </div>

		        <div class="ibox-content">
		            <div class="row">
		                
		                <div class="col-xs-12">
		                    <div class="input-group">
		                    	<input type="text" placeholder="Search" class="input-sm form-control"> 
		                    	<span class="input-group-btn">
		                        	<button type="button" class="btn btn-sm btn-primary"> Go!</button> 
		                        </span>
		                    </div>
		                </div>
		            </div>
		            <hr>
		            <div class="table-responsive">
		                <table class="table table-striped">
		                    <thead>
		                    <tr>

		                        <th>#</th>
		                        <th>Project </th>
		                        <th>Name </th>
		                        <th>Phone </th>
		                        <th>Company </th>
		                        <th>Completed </th>
		                        <th>Task</th>
		                        <th>Date</th>
		                        <th>Action</th>
		                    </tr>
		                    </thead>
		                    <tbody>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>3</td>
		                        <td>Betha project</td>
		                        <td>John Smith</td>
		                        <td>0800 1111</td>
		                        <td>Erat Volutpat</td>
		                        <td><span class="pie">3,1</span></td>
		                        <td>75%</td>
		                        <td>Jul 18, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>3</td>
		                        <td>Betha project</td>
		                        <td>John Smith</td>
		                        <td>0800 1111</td>
		                        <td>Erat Volutpat</td>
		                        <td><span class="pie">3,1</span></td>
		                        <td>75%</td>
		                        <td>Jul 18, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>2</td>
		                        <td>Alpha project</td>
		                        <td>Alice Jackson</td>
		                        <td>0500 780909</td>
		                        <td>Nec Euismod In Company</td>
		                        <td><span class="pie">6,9</span></td>
		                        <td>40%</td>
		                        <td>Jul 16, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>1</td>
		                        <td>Project <small>This is example of project</small></td>
		                        <td>Patrick Smith</td>
		                        <td>0800 051213</td>
		                        <td>Inceptos Hymenaeos Ltd</td>
		                        <td><span class="pie">0.52/1.561</span></td>
		                        <td>20%</td>
		                        <td>Jul 14, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    <tr>
		                        <td>4</td>
		                        <td>Gamma project</td>
		                        <td>Anna Jordan</td>
		                        <td>(016977) 0648</td>
		                        <td>Tellus Ltd</td>
		                        <td><span class="pie">4,9</span></td>
		                        <td>18%</td>
		                        <td>Jul 22, 2013</td>
		                        <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
		                    </tr>
		                    </tbody>
		                </table>
		            </div>

		        </div>
    		
    		</div>
    	</div>

    	{{-- Pedidos en linea --}}
    	<div class="col-xs-12 col-md-4">
    		<div class="ibox">

    			<div class="ibox-title">
		            <h5>Vendedores</h5>
		            <div class="ibox-tools">
		                <a class="collapse-link">
		                    <i class="fa fa-chevron-up"></i>
		                </a>
		                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
		                    <i class="fa fa-wrench"></i>
		                </a>
		                <ul class="dropdown-menu dropdown-user">
		                    <li><a href="#">Config option 1</a>
		                    </li>
		                    <li><a href="#">Config option 2</a>
		                    </li>
		                </ul>
		                <a class="close-link">
		                    <i class="fa fa-times"></i>
		                </a>
		            </div>
		        </div>

		        <div class="ibox-content">
		            <div class="row">
		                
		                <div class="col-xs-12">
		                    <div class="input-group">
		                    	<input type="text" placeholder="Search" class="input-sm form-control"> 
		                    	<span class="input-group-btn">
		                        	<button type="button" class="btn btn-sm btn-primary"> Go!</button> 
		                        </span>
		                    </div>
		                </div>
		                
		            </div>
		            <hr>
		            <table class="table table-striped table-hover">
                        <tbody>
                            <tr>
                                <td class="client-avatar"><img alt="image" src="img/a2.jpg"> </td>
                                <td><a data-toggle="tab" href="#contact-1" class="client-link">Anthony Jackson</a></td>
                                <td> Tellus Institute</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><img alt="image" src="img/a3.jpg"> </td>
                                <td><a data-toggle="tab" href="#contact-2" class="client-link">Rooney Lindsay</a></td>
                                <td>Proin Limited</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><img alt="image" src="img/a4.jpg"> </td>
                                <td><a data-toggle="tab" href="#contact-3" class="client-link">Lionel Mcmillan</a></td>
                                <td>Et Industries</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a5.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-4" class="client-link">Edan Randall</a></td>
                                <td>Integer Sem Corp.</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a6.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-2" class="client-link">Jasper Carson</a></td>
                                <td>Mone Industries</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a7.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-3" class="client-link">Reuben Pacheco</a></td>
                                <td>Magna Associates</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a1.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-1" class="client-link">Simon Carson</a></td>
                                <td>Erat Corp.</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a3.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-2" class="client-link">Rooney Lindsay</a></td>
                                <td>Proin Limited</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a4.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-3" class="client-link">Lionel Mcmillan</a></td>
                                <td>Et Industries</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a5.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-4" class="client-link">Edan Randall</a></td>
                                <td>Integer Sem Corp.</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a2.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-1" class="client-link">Anthony Jackson</a></td>
                                <td> Tellus Institute</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a7.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-2" class="client-link">Reuben Pacheco</a></td>
                                <td>Magna Associates</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a5.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-3"class="client-link">Edan Randall</a></td>
                                <td>Integer Sem Corp.</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a6.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-4" class="client-link">Jasper Carson</a></td>
                                <td>Mone Industries</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a7.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-2" class="client-link">Reuben Pacheco</a></td>
                                <td>Magna Associates</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a1.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-1" class="client-link">Simon Carson</a></td>
                                <td>Erat Corp.</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a3.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-3" class="client-link">Rooney Lindsay</a></td>
                                <td>Proin Limited</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a4.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-4" class="client-link">Lionel Mcmillan</a></td>
                                <td>Et Industries</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a5.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-1" class="client-link">Edan Randall</a></td>
                                <td>Integer Sem Corp.</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a2.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-2" class="client-link">Anthony Jackson</a></td>
                                <td> Tellus Institute</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                            <tr>
                                <td class="client-avatar"><a href=""><img alt="image" src="img/a7.jpg"></a> </td>
                                <td><a data-toggle="tab" href="#contact-4" class="client-link">Reuben Pacheco</a></td>
                                <td>Magna Associates</td>
                                <td>45% <i class="fa fa-level-up"></i></td>
                            </tr>
                        </tbody>
                    </table>

		        </div>
    		
    		</div>
    	</div>

    </div>

</div>

@endsection
	

@section('javascript')



	<!-- Mainly scripts -->
    {{-- <script src="{{ asset('js/backend/jquery-2.1.1.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/backend/bootstrap.min.js') }}"></script> --}}
    <script src="{{ asset('js/backend/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Flot -->
    <script src="{{ asset('js/backend/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/flot/curvedLines.js') }}"></script>

    <!-- Peity -->
    <script src="{{ asset('js/backend/plugins/peity/jquery.peity.min.js') }}"></script>
    <script src="{{ asset('js/backend/demo/peity-demo.js') }}"></script>

    <!-- Custom and plugin javascript -->
    {{-- <script src="{{ asset('js/backend/inspinia.js') }}"></script> --}}
    {{-- <script src="{{ asset('js/backend/plugins/pace/pace.min.js') }}"></script> --}}

    <!-- jQuery UI -->
    <script src="{{ asset('js/backend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Jvectormap -->
    <script src="{{ asset('js/backend/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('js/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

    <!-- Sparkline -->
    <script src="{{ asset('js/backend/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

    <!-- Sparkline demo data  -->
    <script src="{{ asset('js/backend/demo/sparkline-demo.js') }}"></script>

    <!-- ChartJS-->
    <script src="{{ asset('js/backend/plugins/chartJs/Chart.min.js') }}"></script>

    <script>
        $(document).ready(function() {


            // var d1 = [[1262304000000, 6], [1264982400000, 3057], [1267401600000, 20434], [1270080000000, 31982], [1272672000000, 26602], [1275350400000, 27826], [1277942400000, 24302], [1280620800000, 24237], [1283299200000, 21004], [1285891200000, 12144], [1288569600000, 10577], [1291161600000, 10295]];
            // var d2 = [[1262304000000, 5], [1264982400000, 200], [1267401600000, 1605], [1270080000000, 6129], [1272672000000, 11643], [1275350400000, 19055], [1277942400000, 30062], [1280620800000, 39197], [1283299200000, 37000], [1285891200000, 27000], [1288569600000, 21000], [1291161600000, 17000]];

            // var data1 = [
            //     { label: "Data 1", data: d1, color: '#17a084'},
            //     { label: "Data 2", data: d2, color: '#127e68' }
            // ];
            // $.plot($("#flot-chart1"), data1, {
            //     xaxis: {
            //         tickDecimals: 0
            //     },
            //     series: {
            //         lines: {
            //             show: true,
            //             fill: true,
            //             fillColor: {
            //                 colors: [{
            //                     opacity: 1
            //                 }, {
            //                     opacity: 1
            //                 }]
            //             },
            //         },
            //         points: {
            //             width: 0.1,
            //             show: false
            //         },
            //     },
            //     grid: {
            //         show: false,
            //         borderWidth: 0
            //     },
            //     legend: {
            //         show: false,
            //     }
            // });

            var lineData = {
                labels: ["January", "February", "March", "April", "May", "June", "July","August", "September", "October", "November", "Dicember"],
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(5,131,8,0.9)",
                        strokeColor: "rgba(220,220,220,1)",
                        pointColor: "rgba(220,220,220,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: [110, 59, 40, 51, 36, 25, 40, 11, 20, 55, 75, 120]
                    },
                    {
                        label: "Example dataset",
                        fillColor: "rgba(231,183,36,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: [48, 48, 60, 39, 56, 37, 80, 15, 75, 86, 80, 150 ]
                    },
                    // {
                    //     label: "Example dataset",
                    //     fillColor: "rgba(26,179,128,0.5)",
                    //     strokeColor: "rgba(26,179,148,0.7)",
                    //     pointColor: "rgba(26,179,148,1)",
                    //     pointStrokeColor: "#fff",
                    //     pointHighlightFill: "#fff",
                    //     pointHighlightStroke: "rgba(26,179,148,1)",
                    //     data: [48, 98, 60, 39, 16, 37, 30]
                    // },
                    // {
                    //     label: "Example dataset",
                    //     fillColor: "rgba(126,179,148,0.5)",
                    //     strokeColor: "rgba(26,179,148,0.7)",
                    //     pointColor: "rgba(26,179,148,1)",
                    //     pointStrokeColor: "#fff",
                    //     pointHighlightFill: "#fff",
                    //     pointHighlightStroke: "rgba(26,179,148,1)",
                    //     data: [48, 48, 11, 39, 56, 66, 30]
                    // },
                    // {
                    //     label: "Example dataset",
                    //     fillColor: "rgba(26,179,148,0.5)",
                    //     strokeColor: "rgba(26,179,148,0.7)",
                    //     pointColor: "rgba(26,179,148,1)",
                    //     pointStrokeColor: "#fff",
                    //     pointHighlightFill: "#fff",
                    //     pointHighlightStroke: "rgba(26,179,148,1)",
                    //     data: [15, 48, 60, 39, 56, 37, 55]
                    // },
                    // {
                    //     label: "Example dataset",
                    //     fillColor: "rgba(26,119,148,0.5)",
                    //     strokeColor: "rgba(26,179,148,0.7)",
                    //     pointColor: "rgba(26,179,148,1)",
                    //     pointStrokeColor: "#fff",
                    //     pointHighlightFill: "#fff",
                    //     pointHighlightStroke: "rgba(26,179,148,1)",
                    //     data: [10, 48, 60, 39, 56, 40, 30]
                    // }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };


            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

        });
    </script>
@endsection
