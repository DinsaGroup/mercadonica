<!-- Menu -->
@if (Auth::user()->hasRole('agent'))
    @include('backend.controlpanel.partials.agent_menu')
@elseif(Auth::user()->hasRole('admin'))
    @include('backend.controlpanel.partials.admin_menu')
@else
    @include('backend.controlpanel.partials.superadmin_menu')
@endif

<li class="nav-header">
    <div class="dropdown profile-element"> 
        <span class="block m-t-xs">
            
        </span>

        <span class="text-muted text-xs block">
            <div class="info_system">
                <strong class="font-bold">DinamicDIN System</strong>
                <p>Registrado para:<b> {{ Auth::user()->name }}</b><br>Versión: <b>1.0<b></p>
            </div>

        </span> 


    </div>

</li> <!-- /.nav-header -->