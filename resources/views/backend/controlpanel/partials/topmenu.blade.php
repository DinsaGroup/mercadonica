<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " ><i class="fa fa-bars"></i></a>
            
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Buscar algo..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
            
        </div>

        <ul class="nav navbar-top-links navbar-right">

                <li>
                    <a class="right-sidebar-admin" >
                        {{-- {{ Auth::user()->role->get(0)->display_name }} --}}
                        <i class="fa fa-user"></i>
                    </a>
                </li>

                <li style="padding: 0 50px;">
                    <a class="right-sidebar-create">
                        <i class="fa fa-plus"></i> Crear nuevo
                    </a>
                </li>

                {{-- <li>
                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out"></i> Logout
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li> --}}
                                        
                <li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-gears"></i>
                    </a>
                </li>

        </ul>
    </nav>
</div>

<script src="/js/app.js"></script>

<div id="right-sidebar-admin">
    <div class="sidebar-container">
        <div class="tab-content">
            <div id="tab-1" class="tab-pane active">
                <a id="close" class="sidebar-close-admin pull-right">
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                </a>
                <div class="sidebar-title">
                    <h3> <i class="fa fa-user"></i> {{-- {{ Auth::user()->role->get(0)->display_name }} --}}</h3>
                    <small><i class="fa fa-tim"></i> Información general de mi cuenta.</small>
                </div>
                <div class="my_perfil">
                    <div class="photo">
                        <img src="{{ url('images/users/default.png') }}" width="100%" />
                    </div>
                    <div class="content sidebar-title">
                        <span class="detail">Nombre</span><br><span class="data">{{ Auth::user()->name }}</span><br>
                        <span class="detail">Email</span><br><span class="data">{{ Auth::user()->email }}</span><br>
                        <span class="detail">Password</span><br><span class="data">******</span><br>
                        <span class="detail">Cargo</span><br><span class="data">Pendiente</span><br>
                        <span class="detail">Contacto</span><br><span class="data">Pendiente</span><br>
                        {{-- <button><i class="fa fa-pencil-o"></i> Editar</button> --}}
                        <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="right-sidebar-create">
    <div class="sidebar-container">
        <div class="tab-content">
            <div id="tab-2" class="tab-pane active">
                <a id="close" class="sidebar-close-create pull-right">
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                </a>
                <div class="sidebar-title">
                    <h3><i class="fa fa-plus"></i> Crear nuevo</h3>
                    <small><i class="fa fa-tim"></i> Aqui puede seleccionar crear un nuevo item.</small>
                </div>
                <ul class="sidebar-list sidebar-title">              
                    <li><a href="{{ url('MyAdmin/clients/create') }}"><i class="fa fa-plus"></i> Agregar nuevo cliente</a></li>
                    <li><a href="{{ url('MyAdmin/quotes/create') }}"><i class="fa fa-plus"></i> Crear presupuesto</a></li>
                    <li><a href="{{ url('MyAdmin/bills/create') }}"><i class="fa fa-plus"></i> Crear Factura</a></li>  
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="right-sidebar">
    <div class="sidebar-container">
        {{-- <ul class="nav nav-tabs navs-3">
            <li class="">
                <a data-toggle="tab" href="#tab-1">
                    <i class="fa fa-user"></i>
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#tab-2">
                    <i class="fa fa-plus"></i>
                </a>
            </li>
            <li class="">
                <a data-toggle="tab" href="#tab-3">
                    <i class="fa fa-gear"></i>
                </a>
            </li>
        </ul> --}}
        <div class="tab-content">
            <div id="tab-3" class="tab-pane active">
                <a id="close" class="sidebar-close pull-right">
                    <i class="fa fa-times-circle-o" aria-hidden="true"></i>
                </a>
                <div class="sidebar-title">
                    <h3> <i class="fa fa-gears"></i> Configuración</h3>
                    <small><i class="fa fa-tim"></i> Configuración del sistema.</small>
                </div>

                <ul class="sidebar-list sidebar-title">
                    <li><h3><i class="fa fa-building"></i> Empresa</h3>
                        <ul>
                            
                            {{-- <li><a href="{{ url('#') }}">Branding *</a></li> --}}
                            <li><a href="{{ url('MyAdmin/warehouses') }}">Bodegas</a></li>
                            <li><a href="{{ url('MyAdmin/branches') }}">Sucursales</a></li>
                            <li><a href="{{ url('MyAdmin/jobtitles') }}">Puestos</a></li>
                        </ul>
                    </li>
                    <li><h3><i class="fa fa-file-text"></i> Facturación</h3>    
                        <ul>
                            
                            <li><a href="{{ url('MyAdmin/currencies') }}"> Monedas</a></li>
                            <li><a href="{{ url('MyAdmin/banks') }}"> Bancos</a></li>
                            <li><a href="{{ url('MyAdmin/measure-units') }}">Unidades de Medidas</a></li>
                            <li><a href="{{ url('MyAdmin/product-categories') }}">Categorías de Productos</a></li>
                            <li><a href="{{ url('MyAdmin/discounts') }}">Descuentos</a></li>
                            <li><a href="{{ url('MyAdmin/delivery-orders') }}">Orden de entrega</a></li>
                            <li><a href="{{ url('MyAdmin/taxes') }}">Impuestos</a></li>
                            <li><a href="{{ url('MyAdmin/bill-types') }}">Tipos de factura</a></li>
                        </ul>
                    </li>
                    <li><h3><i class="fa fa-users"></i> Staff Administradores</h3>
                        <ul>
                            
                            <li><a href="{{ url('MyAdmin/brands') }}">Branding</a></li>
                            <li><a href="{{ url('MyAdmin/warehouses') }}">Bodegas</a></li>
                            <li><a href="{{ url('MyAdmin/branches') }}">Sucursales</a></li>
                            <li><a href="{{ url('MyAdmin/jobtitles') }}">Puestos</a></li>
                        </ul>
                    </li>
                    <li><h3><i class="fa fa-cogs"></i> Generalidades</h3>
                        <ul>
                            
                            <li><a href="{{ url('MyAdmin/business-categories') }}">Sectores de Mercado</a></li>
                            <li><a href="{{ url('MyAdmin/payment-methods') }}">Métodos de pago</a></li>
                            <li><a href="{{ url('MyAdmin/service-categories') }}">Categoría de servicios</a></li>
                            <li><a href="{{ url('MyAdmin/status') }}">Status</a></li>
                        </ul>
                    </li>
                </ul>

            </div>     
        </div>
    </div>
</div>