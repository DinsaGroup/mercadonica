

<!-- Dashboard -->
<li @if (array_get($menu,'level_1') === 'dashboard') class="active" @endif>
    <a href="{{ url('MyAdmin/') }}"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard</span></a>
</li>

<!-- Clients -->
<li @if (array_get($menu,'level_1') === 'clients') class="active" @endif>
    <a href="{{ url('MyAdmin/clients') }}"><i class="fa fa-industry"></i> <span class="nav-label">Clientes</span></a>
</li>

<!-- Bills -->
<li @if (array_get($menu,'level_1') === 'bills') class="active" @endif>
    <a href="#"><i class="fa fa-file-text"></i> <span class="nav-label">Facturación</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li @if (array_get($menu,'level_2') === 'transactions') class="active" @endif><a href="{{ url('#') }}">Transacciones</a></li>
        <li @if (array_get($menu,'level_2') === 'bills') class="active" @endif><a href="{{ url('#') }}">Facturas <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
                <li @if (array_get($menu, 'level_3') === 'all') class="active" @endif><a href="{{ url('MyAdmin/bills/create') }}">Crear nueva</a></li>
                <li><a href="{{ url('#') }}">Todas</a></li>
                <li><a href="{{ url('MyAdmin/bills/paid') }}">Pagadas</a></li>
                <li><a href="{{ url('MyAdmin/bills/pending') }}">Pendientes</a></li>
                {{-- <li><a href="{{ url('#') }}">Vencidas</a></li> --}}
                <li><a href="{{ url('#') }}">Abonadas</a></li>
                <li><a href="{{ url('#') }}">Recaudación</a></li>
                <li><a href="{{ url('MyAdmin/bills/lost') }}">Perdidas</a></li>
            </ul>
        </li>
        
        <li @if (array_get($menu,'level_2') === 'quotes') class="active" @endif><a href="{{ url('#') }}">Pedido <span class="fa arrow"></span></a>

            <ul class="nav nav-third-level">
                <li><a href="{{ url('MyAdmin/quotes/create') }}">Crear nuevo</a></li>
                <li><a href="{{ url('MyAdmin/quotes/all') }}">Todos</a></li>
                <li><a href="{{ url('MyAdmin/quotes/valid') }}">Pendientes</a></li>
                <li><a href="{{ url('MyAdmin/quotes/expired') }}">Cancelados</a></li>
            </ul>
        </li>
        <li><a href="{{ url('#') }}">Presupuestos <span class="fa arrow"></span></a>

            <ul class="nav nav-third-level">
                <li><a href="{{ url('MyAdmin/quotes/create') }}">Crear nuevo</a></li>
                <li><a href="{{ url('MyAdmin/quotes/all') }}">Todos</a></li>
                <li><a href="{{ url('MyAdmin/quotes/valid') }}">Válidos</a></li>
                <li><a href="{{ url('MyAdmin/quotes/expired') }}">Vencidos</a></li>
                <li><a href="{{ url('MyAdmin/quotes/approved') }}">Aprobados</a></li>
            </ul>
        </li>
        <li><a href="{{ url('#') }}">Facturación recurrente</a></li>
    </ul>
</li>

<!-- Products and services -->
<li @if (array_get($menu,'level_1') === 'catalogo') class="active" @endif>
    <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Catálogo</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li @if (array_get($menu,'level_2') === 'products') class="active" @endif ><a href="{{ url('MyAdmin/products') }}">Productos</a></li>
        <li @if (array_get($menu,'level_2') === 'services') class="active" @endif ><a href="{{ url('MyAdmin/services') }}">Servicios</a></li>
    </ul>
</li>


<!-- Reports -->
<li @if (array_get($menu,'level_1') === 'reports') class="active" @endif>
    <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Reportería</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('#') }}">Ventas</a></li>
        <li><a href="{{ url('#') }}">Compras</a></li>
        <li @if (array_get($menu,'level_2') === 'stock') class="active" @endif ><a href="{{ url('#') }}">Inventario <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
                <li @if (array_get($menu,'level_3') === 'stock-all') class="active" @endif ><a href="{{ url('MyAdmin/stock') }}">Todos los productos</a></li>
                <li><a href="{{ url('#') }}">Por bodega</a></li>
                <li><a href="{{ url('#') }}">Por sucursal</a></li>
                <li><a href="{{ url('#') }}">Por producto</a></li>
                <li><a href="{{ url('#') }}">Productos descontinuados</a></li>
            </ul>

        </li>
    </ul>
</li>


<!-- Expenses and providers -->
<li @if (array_get($menu,'level_1') === 'expenses') class="active" @endif>
    <a href="{{ url('MyAdmin/providers') }}"><i class="fa fa-money"></i> <span class="nav-label">Compras</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li @if (array_get($menu,'level_2') === 'bills') class="active" @endif ><a href="{{ url('#') }}">Facturas de compras <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
                <li><a href="{{ url('MyAdmin/expenses') }}">Todas mis compras</a></li>
                <li><a href="{{ url('MyAdmin/expenses/create') }}">Ingresar factura</a></li>
            </ul>
        </li>

        <li @if (array_get($menu,'level_2') === 'providers') class="active" @endif ><a href="{{ url('#') }}">Proveedores <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
                <li><a href="{{ url('MyAdmin/providers') }}">Lista de proveedores</a></li>
                <li><a href="{{ url('MyAdmin/providers/create') }}">Crear proveedor</a></li>
                <li><a href="{{ url('#') }}">Mis pedidos<span class="fa arrow"></span> </a>
                    <ul class="nav nav-third-level">
                        <li><a href="{{ url('#') }}">Hacer pedido a proveedor</a></li>
                        <li><a href="{{ url('#') }}">Todos los pedidos</a></li>
                        <li><a href="{{ url('#') }}">Pedidos pendientes</a></li>
                        <li><a href="{{ url('#') }}">Pedidos cancelados</a></li>
                    </ul>

                </li>
                
            </ul>

        </li>
    </ul>
</li>

