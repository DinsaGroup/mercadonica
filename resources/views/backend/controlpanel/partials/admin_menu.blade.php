


<!-- Menu -->
<li @if ($sectionPart === 'dashboard') class="active" @endif>
    <a href="{{ url('MyAdmin/') }}"><i class="fa fa-dashboard"></i> <span class="nav-label">Dashboard Admin</span></a>
</li>


<li @if ($sectionPart === 'restaurants') class="active" @endif>
    <a href="#"><i class="fa fa-industry"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('MyAdmin/branches') }}">Listar Clientes</a></li>
        <li><a href="{{ url('#') }}">Agregar Clientes</a></li>
    </ul>
</li>

<li @if ($sectionPart === 'dishes') class="active" @endif>
    <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Productos</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('#') }}">Listar Productos</a></li>
        <li><a href="{{ url('#') }}">Agregar Productos</a></li>
        <li @if ($sectionPart === 'reports') class="active" @endif>
    <a href="#"><i class="fa fa-building-o"></i> <span class="nav-label">Inventario</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('#') }}">Listar Bodegas</a></li>
        <!-- <li><a href="{{ url('#') }}">Agregar Vendedores</a></li> -->
    </ul>
    </ul>
</li>

<li @if ($sectionPart === 'users') class="active" @endif>
    <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Cotizaciones</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('#') }}">Listar Cotizaciones</a></li>
        <li><a href="{{ url('#') }}">Agregar Cotizaciones</a></li>
    </ul>
</li>

<li @if ($sectionPart === 'orders') class="active" @endif>
    <a href="#"><i class="fa fa-file-text"></i> <span class="nav-label">Facturas</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('#') }}">Listar Facturas</a></li>
        <li><a href="{{ url('#') }}">Agregar Facturas</a></li>
    </ul>
</li>

<!--
@if ($sectionPart === 'comments')
    <li class="active">
@else
    <li>
@endif
    <a href="#"><i class="fa fa-comments-o"></i> <span class="nav-label">Comentarios</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="#">Comentarios Aprobados</a></li>
        <li><a href="#">Comentarios Borrados</a></li>
    </ul>

</li>

-->

<li @if ($sectionPart === 'reports') class="active" @endif>
    <a href="#"><i class="fa fa-bullhorn"></i> <span class="nav-label">Vendedores</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('#') }}">Listar Venderdores</a></li>
        <li><a href="{{ url('#') }}">Agregar Vendedores</a></li>
    </ul>

</li>

{{-- Parte de los cupones --}}
{{-- 
<li @if ($sectionPart === 'reports') class="active" @endif>
    <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Cupones</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('admin/coupons/create') }}">Agregar Cupones</a></li>
        <li><a href="{{ url('admin/coupons/show') }}">Listar Cupones</a></li>
    </ul>

</li> --}}

<li @if ($sectionPart === 'cms') class="active" @endif>
    <a href="#"><i class="fa fa-user-plus"></i> <span class="nav-label">Proveedores</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <!-- <li><a href="{{ url('admin/sections') }}">Categorias</a></li> -->
        <li><a href="{{ url('#') }}">Listar Proveedores</a></li>
        <li><a href="{{ url('#') }}">Agregar Proveedores</a></li>
        <!-- <li><a href="{{ url('admin/images') }}">Imágenes</a></li> -->
        <!-- <li><a href="{{ url('admin/faqs') }}">Preguntas Frecuentes</a></li> -->
        <!-- <li><a href="{{ url('admin/videos') }}">Videos</a></li> -->
    </ul>
</li>

<!-- <li @if ($sectionPart === 'cms') class="active" @endif>
    <a href="#"><i class="fa fa-list-alt"></i> <span class="nav-label">Contenido Web</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{ url('admin/sections') }}">Categorias</a></li>
        <li><a href="{{ url('admin/articles') }}">Artículos</a></li>
        <li><a href="{{ url('admin/banners') }}">Slideshow</a></li>
        <li><a href="{{ url('admin/images') }}">Imágenes</a></li> 
        <li><a href="{{ url('admin/faqs') }}">Preguntas Frecuentes</a></li> 
        <li><a href="{{ url('admin/videos') }}">Videos</a></li> 
    </ul>
</li> -->

<li @if ($sectionPart === 'setup') class="active" @endif>
    <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Configuración </span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">


            <li  @if (isset($submenu) and $submenu === 'settings') class="active" @endif>

            <a href="#">Ajustes <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
               <!--  {{-- <li @if (isset($submenu2) and $submenu2 === 'zones') class="active" @endif>
                    <a href="#">Mapa de zonas <span class="fa arrow"></span></a>
                    <ul class="nav nav-four-level">
                        <li><a href="{{ url('#') }}" >Paises</a></li>
                        <li><a href="{{ url('#') }}" >Ciudades</a></li>
                        <li><a href="{{ url('#') }}" >Municipios</a></li>
                        <li><a href="{{ url('#') }}" >Zonas</a></li>
                    </ul>
                </li>  --}}
                {{-- <li><a href="{{ url('#') }}">Cupones de descuento</a></li> --}} -->
                <li><a href="{{ url('#') }}">Monedas</a></li>
                <li><a href="{{ url('#') }}">Bancos</a></li>
                <li><a href="{{ url('#') }}">Unidades de Medidas</a></li>
                <!-- <li><a href="{{ url('#') }}">Niveles de Acceso</a></li> -->
                <li><a href="{{ url('#') }}">Branding</a></li>
                <li><a href="{{ url('#') }}">Puestos</a></li>
                <li><a href="{{ url('#') }}">Sectores de Mercado</a></li>
                <li><a href="{{ url('#') }}">Categorías de Productos</a></li>
                <li><a href="{{ url('#') }}">Bodegas</a></li>
                <!-- <li><a href="{{ url('admin/priceOptions') }}">Opciones del precio</a></li> -->
            </ul>
        </li>

        @if (isset($submenu) and $submenu === 'admin')
            <li class="active">
        @else
            <li>
        @endif
            <a href="#">Staff Administradores <span class="fa arrow"></span></a>
            <ul class="nav nav-third-level">
                <!-- <li><a href="{{ url('#') }}">Super Admins</a></li>
                <li><a href="{{ url('#') }}">Restaurantes</a></li> -->
                <li><a href="{{ url('#') }}">Roles Administrador</a></li>
            </ul>
        </li>
    </ul>
</li>
