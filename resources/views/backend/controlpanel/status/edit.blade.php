@extends('layouts.backend')

@section('title') {{ $title }} @endsection

@section('content')
<!-- Header page / Titulo de la seccion -->
<div class="row wrapper page-heading"> 
    {!! Form::open([
        'method'=>'DELETE',
        'url' => ['MyAdmin/status', $status->id],
        'style' => 'display:inline'
    ]) !!}
    
        <div class="form-group">
            <div class="pull-right">
                
                    <button type="submit" class="btn btn-danger"><i class="fa fa-remove"></i> Eliminar status</button>
                    {{-- {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-xs']) !!} --}}
                
            </div>
        </div>

    {!! Form::close() !!}
    
    <h1> {{ $title }}</h1>
    <small>Edite los datos del status, note que hay datos que son necesarios para guardar los cambios.</small>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="ibox float-e-margins">
            <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

    
                {!! Form::model($status, [
                    'method' => 'PATCH',
                    'url' => ['MyAdmin/status', $status->id],
                    'class' => 'form-horizontal',
                ]) !!}
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', 'Titulo del estado : ', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        
                       
                        <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                            {!! Form::label('description', 'Descripción: ', ['class' => ' control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
                            {!! Form::label('type', 'Tipo de status: ', ['class' => ' control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::select('type', ['bodega' => 'Bodega', 'cliente' => 'Cliente', 'consignacion' => 'Consignacion', 'cotizacion' => 'Cotizacion', 'detalle de orden de compra' => 'Detalle de Orden de Compra','devolucion' => 'Devolucion', 'factura' => 'Factura', 'inventario' => 'Inventario', 'orden de compra' => 'Orden de Compra' , 'orden de entrega' => 'Orden de Entrega', 'producto' => 'Producto', 'proveedor' => 'Proveedor', 'servicio' => 'Servicio', 'usuario' => 'Usuario'], null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 col-sm-offset-3">
                                {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                            </div>
                            <div class=" col-sm-3">
                                <a href="{{ url('MyAdmin/status') }}" class="btn btn-primary cancel" > Cancelar </a>
                            </div>
                        </div>

                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

            </div> <!-- Termina el contenido de la seccion --> 
        </div> <!-- Termina el ibox --> 
    </div> <!-- Termina el row --> 
</div> <!-- Termina el wrapper --> 

@endsection
