@extends('layouts.backend')

@section('title') {{ $title }} @endsection

@section('content')
<!-- Header page / Titulo de la seccion -->
<div class="row wrapper page-heading"> 
    <h1>{{ $title }} <a href="{{ url('MyAdmin/currencies') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-closed"></i> Cancelar</a> </h1>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="ibox float-e-margins">
            <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

                 {!! Form::open(['url' => 'MyAdmin/currencies', 'class' => 'form-horizontal', 'method'=>'POST']) !!}


                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', 'Nombre de la moneda : ', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        
                       
                        <div class="form-group {{ $errors->has('symbol') ? 'has-error' : ''}}">
                            {!! Form::label('symbol', 'Símbolo: ', ['class' => ' control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('symbol', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('symbol', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 col-sm-offset-3">
                                {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                            </div>
                            <div class=" col-sm-3">
                                <a href="{{ url('MyAdmin/currencies') }}" class="btn btn-primary cancel" > Cancelar </a>
                            </div>
                        </div>

                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

            </div> <!-- Termina el contenido de la seccion --> 
        </div> <!-- Termina el ibox --> 
    </div> <!-- Termina el row --> 
</div> <!-- Termina el wrapper --> 

@endsection

