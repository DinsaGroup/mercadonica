@extends('layouts.backend')

@section('title') {{ $title }} @endsection

@section('content')
<!-- Header page / Titulo de la seccion -->
<div class="row wrapper page-heading"> 
    <h1>Monedas <a href="{{ url('MyAdmin/currencies/create') }}" class="btn btn-primary pull-right btn-sm"><i class="fa fa-plus"></i> Agregar Nuevo</a></h1>
    <small>Lista de {{ $title }}, con datos actualizados al {{ date('j.m.o h:i:s A') }}</small>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="ibox float-e-margins">
            <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

    
   	 			<p>Listado de Monedas</p>

   	 			<div class="table-responsive"> {{-- update --}}
                    <table class="table table-striped table-bordered table-hover dataTables">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Símbolo</th>
                                <th>Slug</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
	                        {{-- */$x=0;/* --}}
	                        @foreach($currencies as $item)
	                            {{-- */$x++;/* --}}
	                            <tr class="gradeA">
	                                <td>{{ $item->id }}</a></td>
	                                
	                                <td>{{ $item->name }}</a></td>
	                                <td>{{ $item->symbol }}</td>
	                                <td>{{ $item->slug }}</td>
	                                
	                                <td>
	                                    <a href="{{ url('MyAdmin/currencies/' . $item->id . '/edit') }}" class="details pull-right">
	                                        <i class="fa fa-pencil"></i>
	                                    </a> 
	                                </td>
	                            </tr>
	                        @endforeach
                        </tbody>
                    </table>
                    <div class="pagination"></div>
                </div>

   	 		</div> <!-- Termina el contenido de la seccion --> 
        </div> <!-- Termina el ibox --> 
    </div> <!-- Termina el row --> 
</div> <!-- Termina el wrapper --> 

@endsection

@section('javascript')

<!-- Data Tables -->
    <script src="{{{asset('js/backend/dataTables/jquery.dataTables.js')}}}"></script>
    <script src="{{{asset('js/backend/dataTables/dataTables.bootstrap.js')}}}"></script>
    <script src="{{{asset('js/backend/dataTables/dataTables.responsive.js')}}}"></script>
    <script src="{{{asset('js/backend/dataTables/dataTables.tableTools.min.js')}}}"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function() {
        $('.dataTables').dataTable({
            responsive: true,
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "{{{asset('js/backend/dataTables/swf/copy_csv_xls_pdf.swf')}}}"
            }
        });
    });
  
</script>

@endsection