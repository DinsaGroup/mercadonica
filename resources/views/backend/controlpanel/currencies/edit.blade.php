@extends('layouts.backend')

@section('title') {{ $title }} @endsection

@section('content')
<!-- Header page / Titulo de la seccion -->
<div class="row wrapper page-heading"> 
    {!! Form::open([
        'method'=>'DELETE',
        'url' => ['MyAdmin/currencies', $currency->id],
        'style' => 'display:inline'
    ]) !!}
    
        <div class="form-group">
            <div class="pull-right">
                
                    <button type="submit" class="btn btn-danger"><i class="fa fa-remove"></i> Eliminar moneda</button>
                    {{-- {!! Form::submit('Eliminar', ['class' => 'btn btn-danger btn-xs']) !!} --}}
                
            </div>
        </div>

    {!! Form::close() !!}
    
    <h1> {{ $title }}</h1>
    <small>Edite los datos de la moneda, note que hay datos que son necesarios para guardar los cambios.</small>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row"> 
        <div class="ibox float-e-margins">
            <div class="ibox-content"> <!-- comienza el contenido de la seccion -->

    
                {!! Form::model($currency, [
                    'method' => 'PATCH',
                    'url' => ['MyAdmin/currencies', $currency->id],
                    'class' => 'form-horizontal',
                    'files'=>true
                ]) !!}
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                            {!! Form::label('name', 'Titulo del puesto : ', ['class' => 'control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        
                       
                        <div class="form-group {{ $errors->has('symbol') ? 'has-error' : ''}}">
                            {!! Form::label('symbol', 'Símbolo: ', ['class' => ' control-label col-sm-3']) !!}
                            <div class="col-sm-6">
                                {!! Form::text('symbol', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                {!! $errors->first('symbol', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-3 col-sm-offset-3">
                                {!! Form::submit('Guardar & cerrar', ['class' => 'btn btn-primary form-control create']) !!}
                            </div>
                            <div class=" col-sm-3">
                                <a href="{{ url('MyAdmin/currencies') }}" class="btn btn-primary cancel" > Cancelar </a>
                            </div>
                        </div>

                {!! Form::close() !!}

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

            </div> <!-- Termina el contenido de la seccion --> 
        </div> <!-- Termina el ibox --> 
    </div> <!-- Termina el row --> 
</div> <!-- Termina el wrapper --> 

@endsection
