<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesTable extends Migration
{
    public function up()
    {
        // Tipo de impuestos del sistema
        Schema::create('taxes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->float('amount');
            $table->string('description')->nullable();
            $table->string('slug');
            $table->string('visible')->default('true');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('taxes');
    }
}
