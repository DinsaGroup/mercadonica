<?php

use Illuminate\Database\Seeder;

use App\Role;
use App\User;
use App\Permission;

class UsersTableSeeder extends Seeder
{
    
    public function run()
    {
        // Insert a global admin user
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('role_user')->delete();
        DB::table('permission_role')->delete();
        $users = [
            ['name' => 'Super Control', 'email' => 'catoruno@dinsagroup.com', 'password' => bcrypt('*supercontrol!123*')],
            ['name' => 'Administrador', 'email' => 'admin@dinsagroup.com', 'password' => bcrypt('*admin!123*')],
            ['name' => 'Soporte', 'email' => 'support@dinsagroup.com', 'password' => bcrypt('*support!123*')],
            ['name' => 'Ana Delf', 'email' => 'velas@aromaticas.com', 'password' => bcrypt('*client!123*')],
            ['name' => 'Carlos Toruno', 'email' => 'catoruno@gmail.com', 'password' => bcrypt('*123aa456bb*')],
        ];
        DB::table('users')->insert($users);

        // Create Global User Roles
        $super_admin_role = new Role();
        $super_admin_role->name         		= 'superadmin';
        $super_admin_role->display_name 		= 'Super Administrador'; // optional
        $super_admin_role->description  		= 'Este usuario puede crear administradores del portal, asi como todos los permisos del sistema.'; // optional
        $super_admin_role->save();

        $manager = new Role();
        $manager->name 				= 'manager';
        $manager->display_name 		= 'Administrador general';
        $manager->description  		= 'Este usuario puede crear nuevas cuentas, y asignar ciertos permisos dentro de la plataforma.';
        $manager->save();

        $support = new Role();
        $support->name 				= 'support';
        $support->display_name 		= 'Soporte técnico';
        $support->description  		= 'Este usuario se encarga de brindar asistencia técnica a los clientes y generar reportes de incidencia.';
        $support->save();

        $client = new Role();
        $client->name 				= 'client';
        $client->display_name 		= 'Administrador';
        $client->description  		= 'Este usuario es el dueño de un negocio que promociona sus productos en la plataforma.';
        $client->save();

        $new_user = new Role();
        $new_user->name 				= 'user';
        $new_user->display_name 		= 'Usuario';
        $new_user->description  		= 'Este usuario es quien hace uso de la plataforma para comprar productos.';
        $new_user->save();



        // Create Global user permission
        // $newpermission = new Permission();
        // $newpermission->name         = 'admin-rests';
        // $newpermission->display_name = 'Administrar Restaurantes'; 
        // $newpermission->description  = 'Administrar Restaurantes'; 
        // $newpermission->save();

        // $adminFull = new Permission();
        // $adminFull->name         = 'admin-full';
        // $adminFull->display_name = 'Administrar Todo'; 
        // $adminFull->description  = 'Administrar Todo'; 
        // $adminFull->save();


        // Add role to Admin User
        $user = User::where('email', '=', 'catoruno@dinsagroup.com')->first();
        $user->roles()->attach($super_admin_role);

        $user = User::where('email', '=', 'admin@dinsagroup.com')->first();
        $user->roles()->attach($manager);

        $user = User::where('email', '=', 'support@dinsagroup.com')->first();
        $user->roles()->attach($support);

        $user = User::where('email', '=', 'velas@aromaticas.com')->first();
        $user->roles()->attach($client);

        $user = User::where('email', '=', 'catoruno@gmail.com')->first();
        $user->roles()->attach($new_user);

        // $admin->attachPermission($adminFull);
        // $owner->attachPermission($newpermission);
    }
}
