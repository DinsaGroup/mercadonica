<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillType extends Model
{
    protected $tables = 'bill_types';

    protected $fillable = ['name','slug',];
}
