<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    protected $tables = 'job_titles';

    protected $fillable = ['name','description','slug'];
}
