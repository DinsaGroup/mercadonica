<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $tables = 'statuses';

    protected $fillable = ['name','description','slug', 'type', 'visible'];
}
