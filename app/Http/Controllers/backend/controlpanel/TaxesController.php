<?php

namespace App\Http\Controllers\backend\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Tax;
use Auth;

class TaxesController extends Controller
{
    public function index()
    {
        try{
            $taxes = Tax::where('visible','true')->OrderBy('name','asc')->get();
            return view('backend.controlpanel.taxes.index',[
                    'title' => 'Impuestos',
                    'menu' => $this->menu(),
                    'taxes' => $taxes,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            return view('backend.controlpanel.taxes.create',[
                    'title' => 'Crear nuevo impuesto',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $taxes = new Tax;
            $taxes->name = $request->name;
            $taxes->description = $request->description;
            $taxes->amount = $request->amount;
            $taxes->slug = str_slug($request->name,"-");
            $taxes->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $taxes->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $taxes->save();

            \Session::flash('success_message','¡El impuesto se ha creado con éxito!');
            return redirect('MyAdmin/taxes');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{
            $tax = Tax::find($id);
            return view('backend.controlpanel.taxes.edit',[
                    'title' => 'Editar impuesto',
                    'menu' => $this->menu(),
                    'tax' => $tax,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $taxes = Tax::find($id);
            $taxes->name = $request->name;
            $taxes->description = $request->description;
            $taxes->amount = $request->amount;
            $taxes->slug = str_slug($request->name,"-");
            $taxes->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $taxes->update();

            \Session::flash('success_message','¡El impuesto se ha actualizado con éxito!');
            return redirect('MyAdmin/taxes');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $tax = Tax::find($id);
            $tax->visible = 'false';
            $tax->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $tax->update();

            \Session::flash('success_message','¡El impuesto se ha eliminado con éxito!');
            return redirect('MyAdmin/taxes');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
