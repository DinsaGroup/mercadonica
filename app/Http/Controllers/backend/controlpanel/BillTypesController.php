<?php

namespace App\Http\Controllers\backend\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\BillType;
use Auth;

class BillTypesController extends Controller
{
    public function index()
    {
        try{
            $billtypes = BillType::where('visible','true')->OrderBy('name','asc')->get();
            return view('backend.controlpanel.billTypes.index',[
                    'title' => 'Tipos de Factura',
                    'menu' => $this->menu(),
                    'billtypes' => $billtypes,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            return view('backend.controlpanel.billTypes.create',[
                'title' => 'Crear nuevo tipo de factura',
                'menu' => $this->menu(),
            ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $billTypes = new BillType;
            $billTypes->name = $request->name;
            $billTypes->slug = str_slug($request->name,"-");
            $billTypes->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $billTypes->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $billTypes->save();

            \Session::flash('success_message','¡El tipo de factura se ha creado con éxito!');
            return redirect('MyAdmin/bill-types');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{
            $billtype = BillType::find($id);
            return view('backend.controlpanel.billTypes.edit',[
                    'title' => 'Editar tipo de factura',
                    'menu' => $this->menu(),
                    'billtype' => $billtype,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $billtypes = BillType::find($id);
            $billtypes->name = $request->name;
            $billtypes->slug = str_slug($request->name,"-");
            $billtypes->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $billtypes->update();

            \Session::flash('success_message','¡El tipo de factura se ha actualizado con éxito!');
            return redirect('MyAdmin/bill-types');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $billtype = BillType::find($id);
            $billtype->visible = 'false';
            $billtype->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $billtypes->update();

            \Session::flash('success_message','¡El tipo de factura se ha eliminado con éxito!');
            return redirect('MyAdmin/bill-types');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
