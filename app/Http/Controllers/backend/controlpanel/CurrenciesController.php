<?php

namespace App\Http\Controllers\backend\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Currency;
use Auth;

class CurrenciesController extends Controller
{
    public function index()
    {
        try{
            $currencies = Currency::where('visible','true')->OrderBy('name','asc')->get();
            return view('backend.controlpanel.currencies.index',[
                    'title' => 'Monedas',
                    'menu' => $this->menu(),
                    'currencies' => $currencies,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            return view('backend.controlpanel.currencies.create',[
                    'title' => 'Crear nueva moneda',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $currencies = new Currency;
            $currencies->name = $request->name;
            $currencies->symbol = $request->symbol;
            $currencies->slug = str_slug($request->name,"-");
            $currencies->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $currencies->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $currencies->save();

            \Session::flash('success_message','¡La moneda se ha creado con éxito!');
            return redirect('MyAdmin/currencies');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{
            $currency = Currency::find($id);
            return view('backend.controlpanel.currencies.edit',[
                    'title' => 'Editar moneda',
                    'menu' => $this->menu(),
                    'currency' => $currency,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }
    
    public function update(Request $request, $id)
    {
        try{
            $currencies = Currency::find($id);
            $currencies->name = $request->name;
            $currencies->symbol = $request->symbol;
            $currencies->slug = str_slug($request->name,"-");
            $currencies->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $currencies->update();

            \Session::flash('success_message','¡La moneda se ha actualizado con éxito!');
            return redirect('MyAdmin/currencies');

        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }
    
    public function destroy($id)
    {
        try{
            $currency = Currency::find($id);
            $currency->visible = 'false';
            $currency->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $currency->update();

            \Session::flash('success_message','¡La moneda se ha eliminado con éxito!');
            return redirect('MyAdmin/currencies');
        } catch(Exception $e) {
            ssion::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
