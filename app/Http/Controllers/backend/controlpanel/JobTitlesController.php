<?php

namespace App\Http\Controllers\backend\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\JobTitle;
use Auth;

class JobTitlesController extends Controller
{
    public function index()
    {
        try{
            $jobtitles = JobTitle::where('visible','true')->OrderBy('id','asc')->get();
            return view('backend.controlpanel.jobTitles.index',[
                    'title' => 'Puestos',
                    'menu' => $this->menu(),
                    'jobtitles' => $jobtitles,
                ]);
        } catch(Excepction $e) {
            \Session::flash('error_message','¡¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

   
    public function create()
    {
        try{
            return view('backend.controlpanel.jobTitles.create',[
                    'title' => 'Crear nuevo puesto',
                    'menu' => $this->menu(),
                ]);
        } catch(Excepction $e) {
            \Session::flash('error_message','¡¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

   
    public function store(Request $request)
    {
        try{
            $jobtitles = new JobTitle;
            $jobtitles->name = $request->name;
            $jobtitles->description = $request->description;
            $jobtitles->slug = str_slug($request->name,"-");
            $jobtitles->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $jobtitles->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $jobtitles->save();

            \Session::flash('success_message','¡El puesto se ha creado con éxito!');
            return redirect('MyAdmin/jobtitles');
        } catch(Excepction $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function edit($id)
    {
        try{
            $jobtitle = JobTitle::find($id);
            return view('backend.controlpanel.jobTitles.edit',[
                    'title' => 'Editar puesto',
                    'menu' => $this->menu(),
                    'jobtitle' => $jobtitle,
                ]);
        } catch(Excepction $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $jobtitles = JobTitle::find($id);
            $jobtitles->name = $request->name;
            $jobtitles->description = $request->description;
            $jobtitles->slug = str_slug($request->name,"-");
            $jobtitles->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $jobtitles->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $jobtitles->update();

            \Session::flash('success_message','¡El puesto se ha actualizado con éxito!');
            return redirect('MyAdmin/jobtitles');
        } catch(Excepction $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }


    
    public function destroy($id)
    {
        try{
            $jobtitle = JobTitle::find($id);
            $jobtitle->visible = 'false';
            $jobtitle->updated_by =  Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $jobtitle->update();

            \Session::flash('success_message', '¡El puesto se ha eliminado con éxito');
            return redirect('MyAdmin/jobtitles');
        } catch(Excepction $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
