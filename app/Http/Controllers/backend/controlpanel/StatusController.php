<?php

namespace App\Http\Controllers\backend\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Status;
use Auth;

class StatusController extends Controller
{
    public function index()
    {
        try{
            $statuses = Status::where('visible','true')->OrderBy('id','asc')->get();
            return view('backend.controlpanel.status.index',[
                    'title' => 'Status',
                    'menu' => $this->menu(),
                    'statuses' => $statuses,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function create()
    {
        try{
            return view('backend.controlpanel.status.create',[
                    'title' => 'Crear nuevo status',
                    'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function store(Request $request)
    {
        try{
            $statuses = new Status;
            $statuses->name = $request->name;
            $statuses->description = $request->description;
            $statuses->type = $request->type;
            $statuses->slug = str_slug($request->name,"-");
            $statuses->created_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $statuses->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $statuses->save();

            \Session::flash('success_message','¡El status se ha creado con éxito!');
            return redirect('MyAdmin/status');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        try{
            $status = Status::find($id);
            return view('backend.controlpanel.status.edit',[
                    'title' => 'Editar status',
                    'menu' => $this->menu(),
                    'status' => $status,
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {
        try{
            $statuses = Status::find($id);
            $statuses->name = $request->name;
            $statuses->description = $request->description;
            $statuses->type = $request->type;
            $statuses->slug = str_slug($request->name,"-");
            $statuses->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $statuses->update();

            \Session::flash('success_message','¡El status se ha actualizado con éxito!');
            return redirect('MyAdmin/status');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try{
            $status = Status::find($id);
            $status->visible = 'false';
            $status->updated_by = Auth::user()->id .' | '. Auth::user()->name .' | '. $this->ip_address();
            $status->update();

            \Session::flash('success_message','¡El status se ha eliminado con éxito!');
            return redirect('MyAdmin/status');
        } catch(Exception $e) {
            \Session::flash('error_message','¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    private function ip_address(){

        $ip = '0.0.0.0';

        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            $ip = $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            $ip = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"])){
            $ip = $_SERVER["HTTP_FORWARDED"];
        }
        else{
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return $ip;
    }

    private function menu(){
        $menu = [
                'level_1' => '',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
