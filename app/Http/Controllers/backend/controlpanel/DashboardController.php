<?php

namespace App\Http\Controllers\backend\controlpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index()
    {
        try{
            return view('backend.controlpanel.dashboard.index', [
                'title' => 'Dashboard',
                'menu' => $this->menu(),
                ]);
        } catch(Exception $e) {
            \Session::flash('error_message', '¡Hubo un error en la última solicitud!');
            return redirect()->back();
        }
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }

    private function menu(){
        $menu = [
                'level_1' => 'dashboard',
                'level_2' => '',
                'level_3' => '',
                'level_4' => '',
            ];
        return $menu;
    }
}
