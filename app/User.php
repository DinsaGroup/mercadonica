<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    protected $fillable = [
        'name', 'email', 'password','photo','phone',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

     public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function restore(){
        $this->restoreA();
        $this->restoreB();
    }
}
