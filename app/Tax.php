<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $tables = 'taxes';

    protected $fillable = ['name', 'description','amount','slug'];
}
