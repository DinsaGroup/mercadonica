<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $tables = 'currencies';

    protected $fillable = ['name','symbol','slug'];
}
